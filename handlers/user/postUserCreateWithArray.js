import User from '../../utils/dynamo/users';
import crypto from "crypto";

export default async (event) => {
  try {
    const body = JSON.parse(event.body);
    const payload = body.reduce((out, user) => {
        const salt = crypto.randomBytes(16).toString('hex');
        const hash = crypto.pbkdf2Sync(user.password, salt, 1000, 64, `sha512`).toString(`hex`);
        user.hash = hash;
        user.salt = salt;
        delete user.password;
        out = [...out, user];
    }, []);

    const result = await User.batchPut(payload);
    return {
      statusCode: 200,
      body: JSON.stringify(result.map(item => {
          delete item.hash;
          delete item.salt;
          return item;
      })),
    };
  } catch (error) {
    /* something terrible has happened */
    console.error(error);
    return {
        statusCode: 500,
        headers: {},
        body: "Oups, a wild error appeared! Our developers are scrambling to fix this!",
        isBase64Encoded: false
    };
  }
};
