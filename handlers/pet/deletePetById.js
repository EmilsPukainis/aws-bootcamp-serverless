import Pet from "../../utils/dynamo/pets";

export default async (event) => {
  try {
    const petId = event.pathParameters?.petId;
    const pet = await Pet.get(petId);

    if (!pet) {
        return {
            statusCode: 404,
            body: "Pet not found"
        };
    }

    await Pet.delete(pet.id);

    return {
      statusCode: 200,
      headers: {},
      body: JSON.stringify(`deleted user with username: ${petId} (${pet.name})`),
      isBase64Encoded: false
    };
  } catch (error) {
    /* something terrible has happened */
    console.error(error);
    return {
      statusCode: 500,
      headers: {},
      body: "Oups, a wild error appeared! Our developers are scrambling to fix this!",
      isBase64Encoded: false
    };
  }
};
