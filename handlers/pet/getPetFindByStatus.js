import Pet from "../../utils/dynamo/pets";

export default async (event) => {
  try {
    const status = event.queryStringParameters?.status;
    const pets = await Pet.query("status").eq(status).exec();

    if (!pets || pets.count < 1) {
        return {
            statusCode: 404,
            body: "no pets found for status: " + status
        };
    }
    return {
      statusCode: 200,
      headers: {},
      body: JSON.stringify(pets),
      isBase64Encoded: false
    };
  } catch (error) {
    /* something terrible has happened */
    console.error(error);
    return {
      statusCode: 500,
      headers: {},
      body: "Oups, a wild error appeared! Our developers are scrambling to fix this!",
      isBase64Encoded: false
    };
  }
};
