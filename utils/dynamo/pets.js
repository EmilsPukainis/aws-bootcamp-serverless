import { aws, Schema, model } from 'dynamoose';
import { v4 as uuidv4 } from 'uuid';

/* If we are on local - connect to local table */
if (process.env.STAGE === "local") {
  aws.ddb.local(process.env.DYNAMO_URL);
}

/*
  Create the schema - how the table data should look)
  This schema also validates the input data when anything is used in the Model
*/
const  PetSchema = new Schema({
  "id": {
    "type": String,
    required: false,
    "hashKey": true,
    default: uuidv4()
  },
  "category": {
    "type": Object,
    required: true,
    "schema": {
      id: String,
      name: String
    }
  },
  "name": { "type": String, required: true },
  "photoUrls": { "type": Array },
  "tags": { "type": Array },
  "status": {
    "type": String,
    required: true,
    index: {
      name: "PetStatus",
      global: true,
    },
    "rangeKey": true
  },
});


/* verify that all the env variables are set in .env file */
if (!process || !process.env || !process.env.PETS_TABLE_NAME) {
  throw new Error('no Pets table name configured');
}
if (!process || !process.env || !process.env.SERVICE) {
  throw new Error('no service name configured');
}
if (!process || !process.env || !process.env.STAGE) {
  throw new Error('no stage configured');
}

// const stage = process.env?.STAGE;
const service = process.env?.SERVICE;
const stage = process.env?.STAGE;
const table = process.env?.PETS_TABLE_NAME;

/* this is how we access the data in the table, by providing table name and schema */
const Pet = model(`${service}-${stage}-${table}`, PetSchema);

export default Pet;
